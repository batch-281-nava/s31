// using require directive to load http module
let http = require ("http");

// create server 
http.createServer(function (request, response) {
	// returning what type of response being thrown to the client
	response.writeHead(200, {'Content-Type': 'text/plain'});
	// send the response with the text content 'Hello World!'
	response.end('Hello World!');
}).listen(4000);

// When server is running, console will print the message:
console.log('Server is running at localhost:4000');

